package com.presensiToDoList.app.repository;

import com.presensiToDoList.app.model.ToDoList;
import com.presensiToDoList.app.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

//ALUR PEMBUATAN (31)

//membuat repository untuk menjalan query mysql
@Repository
public interface ToDoListReppository extends JpaRepository<ToDoList, Long> {
//    query findByUserId
    @Query(value = "select * from todolist where user_id = :user_id", nativeQuery = true)
    List<ToDoList> findByUserId(Integer user_id);
}
