package com.presensiToDoList.app.repository;

import com.presensiToDoList.app.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

//ALUR PEMBUATAN (5)

//membuat repository untuk mempermudah validasi query mysql
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findByEmail(String email);
    Boolean existsByEmail(String email);
}
