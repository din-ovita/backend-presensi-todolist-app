package com.presensiToDoList.app.repository;

import com.presensiToDoList.app.model.Absen;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

//ALUR PEMBUATAN (38)

//membuat repository untuk menjalankan query mysql
@Repository
public interface AbsenRepository extends JpaRepository<Absen, Long> {
    @Query(value = "select * from absensi where user_id = :user_id", nativeQuery = true)
    Page<Absen> findByUserId(Integer user_id, Pageable pages);
}
