package com.presensiToDoList.app.repository;

import com.presensiToDoList.app.model.TemporaryToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

//ALUR PEMBUATAN (8)

//membuat repository untuk membuat query mysql
@Repository
public interface TemporaryTokenRepository extends JpaRepository<TemporaryToken, Integer> {
    Optional<TemporaryToken> findByToken(String token);
    Optional<TemporaryToken> findByUserId(int userId);
}
