package com.presensiToDoList.app.exception;

//ALUR PEMBUATAN (20)

//membuat validasi kondisi error id tidak ditemukan
public class NotFoundException extends RuntimeException{
    public NotFoundException(String message) {
        super(message);
    }
}
