package com.presensiToDoList.app.exception;

//ALUR PEMBUATAN (21)

//membuat validasi kondisi error internal server
public class InternalErrorException extends RuntimeException{
    public InternalErrorException(String message) {
        super(message);
    }
}
