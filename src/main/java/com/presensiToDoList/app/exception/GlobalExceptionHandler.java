package com.presensiToDoList.app.exception;

import com.presensiToDoList.app.response.ResponseHelper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

//ALUR PEMBUATAN (22)

//membuat kontrol validasi
@ControllerAdvice
public class GlobalExceptionHandler {
//    validasi notfound
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<?> notFound(NotFoundException notFoundException) {
        return ResponseHelper.err(notFoundException.getMessage(), HttpStatus.NOT_FOUND);
    }
//    validasi internal server error
    @ExceptionHandler(InternalErrorException.class)
    public ResponseEntity<?> internalError(InternalErrorException internalErrorException) {
        return ResponseHelper.err(internalErrorException.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
//validasi email telah digunakan
    @ExceptionHandler(EmailCondition.class)
    public ResponseEntity<?> emailCondition(EmailCondition emailCondition) {
        return ResponseHelper.err(emailCondition.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
