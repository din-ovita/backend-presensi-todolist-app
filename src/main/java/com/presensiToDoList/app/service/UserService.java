package com.presensiToDoList.app.service;


import com.presensiToDoList.app.dto.LoginDto;
import com.presensiToDoList.app.dto.UserDto;
import com.presensiToDoList.app.model.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

//ALUR PEMBUATAN (6)

//membuat service untuk membuat method/CRUD
public interface UserService {
//    method login
    Map<String, Object> login(LoginDto loginDto);
//    method register
    User addUser(User user);
//    method update user
    User updateUser(Integer id, User userDto, MultipartFile multipartFile);
//    User updateUser(Integer id, UserDto userDto, MultipartFile multipartFile);
//    method get by id
    User getById(Integer id);
//    method get all
    List<User> getAll();
//    method update foto profil
    User updateImg(Integer id, MultipartFile multipartFile);
}
