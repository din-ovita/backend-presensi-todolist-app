package com.presensiToDoList.app.service;

import com.presensiToDoList.app.dto.ListDto;
import com.presensiToDoList.app.model.ToDoList;

import java.util.List;
import java.util.Optional;

//ALUR PEMBUATAN (32)

//membuat service untuk membuat method CRUD
public interface ToDoListService {
//    method tambah
    ToDoList addList(ListDto list);
//    method edit todolist
    ToDoList updateList(Long id, ListDto list);
//    method kondisi selesai
    ToDoList selesai(Long id);
//    method get all by user id (relasi user)
    List<ToDoList> getUserId(Integer userId);
//    method get id
    Optional<ToDoList> getId(Long id);
//    method delete
    void delete(Long id);
}
