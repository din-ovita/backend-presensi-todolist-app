package com.presensiToDoList.app.service;

import com.presensiToDoList.app.dto.AbsensiDto;
import com.presensiToDoList.app.model.Absen;
import org.springframework.data.domain.Page;

import java.util.Optional;

//ALUR PEMBUATAN (39)

//membuat method CRUD absensi
public interface AbsenService {
//    method add atau tambah absen
    Absen absenYuk(AbsensiDto absensi);
//    method get by user id
//    page untuk menjalankan pageable
    Page<Absen> getAbsen(int page, Integer userId);
//    method get by id
    Optional<Absen> getId(Long id);
//    method delete by id
    void delete(Long id);

}
