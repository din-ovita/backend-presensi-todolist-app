package com.presensiToDoList.app.dto;

//ALUR PEMBUATAN (25)

//membuat dto login
public class LoginDto {
    private String email;
    private String password;

    public LoginDto() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
