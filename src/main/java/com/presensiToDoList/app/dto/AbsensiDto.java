package com.presensiToDoList.app.dto;

import com.presensiToDoList.app.enumating.PresensiEnum;

//ALUR PEMBUATAN (37)

//membuat dto absensi / cetakan model absensi
public class AbsensiDto {
    private Integer userId;
    private PresensiEnum presensi;

    public AbsensiDto() {
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public PresensiEnum getPresensi() {
        return presensi;
    }

    public void setPresensi(PresensiEnum presensi) {
        this.presensi = presensi;
    }
}
