package com.presensiToDoList.app.dto;

//ALUR PEMBUATAN (30)

//membuat dto todolist unutuk mempermudah mengisi data yang hanya perlu diisi manual
public class ListDto {
    private String task;
    private Integer userId;

    public ListDto() {
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
