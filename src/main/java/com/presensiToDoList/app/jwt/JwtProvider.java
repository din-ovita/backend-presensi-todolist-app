package com.presensiToDoList.app.jwt;

import com.presensiToDoList.app.exception.InternalErrorException;
import com.presensiToDoList.app.exception.NotFoundException;
import com.presensiToDoList.app.model.TemporaryToken;
import com.presensiToDoList.app.model.User;
import com.presensiToDoList.app.repository.TemporaryTokenRepository;
import com.presensiToDoList.app.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

//ALUR PEMBUATAN (11)

//membuat token login
@Component
public class JwtProvider {
    private static String secretkey = "presensi app";
    private static Integer expired = 900000;

    @Autowired
    private TemporaryTokenRepository temporaryTokenRepository;

    @Autowired
    private UserRepository userRepository;

    public String generateToken(UserDetails userDetails) {
        String token = UUID.randomUUID().toString().replace("-", "");
        User user = userRepository.findByEmail(userDetails.getUsername()).orElseThrow(() -> new NotFoundException("User not found Generate token"));
        var checkingToken = temporaryTokenRepository.findByUserId(user.getId());
        if (checkingToken.isPresent()) temporaryTokenRepository.deleteById(checkingToken.get().getId());
        TemporaryToken temporaryToken = new TemporaryToken();
        temporaryToken.setToken(token);
        temporaryToken.setExpiredDate(new Date(new Date().getTime() + expired));
        temporaryToken.setUserId(user.getId());
        temporaryTokenRepository.save(temporaryToken);
        return token;
    }
    public TemporaryToken getSubject(String token) {
        return temporaryTokenRepository.findByToken(token).orElseThrow(() -> new InternalErrorException("Token error parse"));
    }

//    untuk mengecek apakah akun tersebut memiliki token atau tidak
    public boolean checkingTokenJwt(String token) {
        TemporaryToken tokenExist = temporaryTokenRepository.findByToken(token).orElse(null);
        if (tokenExist == null) {
            System.out.println("Token kosong");
            return false;
        }
        if (tokenExist.getExpiredDate().before(new Date())) {
            System.out.println("Token expired");
            return false;
        }
        return true;
    }



}
