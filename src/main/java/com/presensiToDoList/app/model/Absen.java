package com.presensiToDoList.app.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.presensiToDoList.app.enumating.PresensiEnum;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

//ALUR PEMBUATAN (36)

//membuat table absensi beserta column nya
@Entity
@Table(name = "absensi")
public class Absen{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    relasi ke id user
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User userId;

//    type date untuk membuat tanggal otomatis
    @JsonFormat(pattern = "dd-MM-yyyy") //membuat format tanggal
    @Column
    @CreationTimestamp
    private LocalDate date;

//    type datetime untuk membuat waktu otomatis
    @JsonFormat(pattern = "HH:mm:ss") //membuat format jam
    @Column(name = "jam")
    private Date jam;

//    enum presensi = masuk atau pulang
    @Enumerated(value = EnumType.STRING)
    @Column(name = "presensi")
    private PresensiEnum presensi;

//    constructor kosong untuk mempermudah pemanggilan method
    public Absen() {
    }

    public Absen(User userId, LocalDate date, Date jam, PresensiEnum presensi) {
        this.userId = userId;
        this.date = date;
        this.jam = jam;
        this.presensi = presensi;
    }

//    getter dan setter
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Date getJam() {
        return jam;
    }

    public void setJam(Date jam) {
        this.jam = jam;
    }

    public PresensiEnum getPresensi() {
        return presensi;
    }

    public void setPresensi(PresensiEnum presensi) {
        this.presensi = presensi;
    }
}
