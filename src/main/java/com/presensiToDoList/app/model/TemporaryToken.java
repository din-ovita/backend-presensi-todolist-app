package com.presensiToDoList.app.model;

import javax.persistence.*;
import java.util.Date;

//ALUR PEMBUATAN (7)

//membuat table temporary token untuk membuat token login
@Entity
@Table(name = "temporary_token")
public class TemporaryToken {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String token;
    @Column(name = "expired_date")
    private Date expiredDate;
    @Column(name = "user_id")
    private int userId;

    public TemporaryToken() {
    }

    public TemporaryToken(String token, Date expiredDate, int userId) {
        this.token = token;
        this.expiredDate = expiredDate;
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
