package com.presensiToDoList.app.model;

import javax.persistence.*;

//ALUR PEMBUATAN (4)

//membuat table user
@Entity
@Table(name = "user")
public class User {
//    membuat column di table user
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "username")
    private String username;

    @Column(name = "no_hp")
    private Long noHp;

    @Lob
    @Column(name = "bio")
    private String bio;

    @Lob
    @Column(name = "profil")
    private String profil;

//    constructor kosong untuk mempermudah pemanggilan method
    public User() {
    }

    public User(String email, String password, String username, Long noHp, String bio, String profil) {
        this.email = email;
        this.password = password;
        this.username = username;
        this.noHp = noHp;
        this.bio = bio;
        this.profil = profil;
    }

//    getter and setter
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getNoHp() {
        return noHp;
    }

    public void setNoHp(Long noHp) {
        this.noHp = noHp;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getProfil() {
        return profil;
    }

    public void setProfil(String profil) {
        this.profil = profil;
    }
}
