package com.presensiToDoList.app.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

//ALUR PEMBUATAN (9)

//membuat implements dari UserDetails untuk membuat system login
public class UserPrinciple implements UserDetails {
    private String email;
    private String password;

    private Collection<? extends GrantedAuthority> authority;

    public UserPrinciple(String email, String password) {
        this.email = email;
        this.password = password;
        this.authority = authority;
    }
    public static UserPrinciple build(User user) {
//        var role = Collections.singletonList(new SimpleGrantedAuthority(user.getId()));
        return new UserPrinciple(
                user.getEmail(),
                user.getPassword()
        );
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authority;
    }



    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
