package com.presensiToDoList.app.controller;

import com.presensiToDoList.app.dto.ListDto;
import com.presensiToDoList.app.model.ToDoList;
import com.presensiToDoList.app.response.CommonResponse;
import com.presensiToDoList.app.response.ResponseHelper;
import com.presensiToDoList.app.service.ToDoListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

//ALUR PEMBUATAN (34)

//menjalankan method dari request client
@RestController
//menandakan bahwa sebuah file adalah controller
@RequestMapping("/list")
public class ToDoListController {
    @Autowired
    private ToDoListService toDoListService;

//    request untuk menjalankan method post atu menambahkan todolist
    @PostMapping
    public CommonResponse<ToDoList> addList(@RequestBody ListDto list) {
        return ResponseHelper.ok(toDoListService.addList(list));
    }

//    request untuk menjalankan method delete by id
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        toDoListService.delete(id);
    };

//    request untuk menjalankan method update todolist
    @PutMapping("/{id}")
    public CommonResponse<ToDoList> updateList(@PathVariable("id") Long id, @RequestBody ListDto list) {
        return ResponseHelper.ok(toDoListService.updateList(id, list));
    }

//    request untuk menjalankan method update validasi selesai
    @PutMapping("/selesai/{id}")
    public CommonResponse<ToDoList> selesai(@PathVariable("id") Long id) {
        return ResponseHelper.ok(toDoListService.selesai(id));
    }

//    request untuk menjalankan method get by user id
//    @RequestParam untuk membuat parameter
    @GetMapping
    public CommonResponse<List<ToDoList>> getUserId(@RequestParam Integer userId) {
        return ResponseHelper.ok(toDoListService.getUserId(userId));
    }

//    request untuk menjalankan nethod get by id
    @GetMapping("/{id}")
    public CommonResponse<Optional<ToDoList>> getId(@PathVariable("id") Long id) {
        return ResponseHelper.ok(toDoListService.getId(id));
    }
}
