package com.presensiToDoList.app.controller;

import com.presensiToDoList.app.dto.AbsensiDto;
import com.presensiToDoList.app.model.Absen;
import com.presensiToDoList.app.response.CommonResponse;
import com.presensiToDoList.app.response.ResponseHelper;
import com.presensiToDoList.app.service.AbsenService;
import com.presensiToDoList.app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

//ALUR PEMBUATAN (41)

//menjalankan sistem atau request dari client
@RestController
@RequestMapping("/absen")
public class AbsenController {
    @Autowired
    private UserService userService;

    @Autowired
    private AbsenService absenService;

//    request untuk menjalankan post
    @PostMapping
    public CommonResponse<Absen> absenYuk(@RequestBody AbsensiDto absensi) {
        return ResponseHelper.ok(absenService.absenYuk(absensi));
    }

//    request untuk menjalankan delete by id
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        absenService.delete(id);
    }

//    request untuk menjalankan get all by id user
    @GetMapping
    public CommonResponse<Page<Absen>> getAbsen(@RequestParam(required = false) int page, @RequestParam Integer userId) {
        return ResponseHelper.ok(absenService.getAbsen(page, userId));
    }

//    request untuk menjalankan get by id
    @GetMapping("/{id}")
    public CommonResponse<Optional<Absen>> getId(@PathVariable("id") Long id) {
        return ResponseHelper.ok(absenService.getId(id));
    }
}
