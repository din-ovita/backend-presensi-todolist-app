package com.presensiToDoList.app.controller;

import com.presensiToDoList.app.dto.LoginDto;
import com.presensiToDoList.app.dto.UserDto;
import com.presensiToDoList.app.model.User;
import com.presensiToDoList.app.response.CommonResponse;
import com.presensiToDoList.app.response.ResponseHelper;
import com.presensiToDoList.app.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

//ALUR PEMBUATAN (28)

//menjalankan method
@RestController
@RequestMapping("/user")
//membuat requestmapping atau membuat path
public class UserController {
//    import model mappper
    @Autowired
    private ModelMapper modelMapper;

//    import file service
    @Autowired
    private UserService userService;

//    request yang menjalankan sistem post atau tambah data
    @PostMapping
    public CommonResponse<User> addUser(UserDto user) {
        return ResponseHelper.ok(userService.addUser(modelMapper.map(user, User.class)));
    };

//    request yang menjalankan sistem update
//    consumes multipart/form-data untuk request file dan ditambahkan juga anotasi RequestPart("file")
//    path digunakan untuk memanggil id dan ditambahkan anotasi PathVariable("id)
    @PutMapping(consumes = "multipart/form-data", path = "/{id}")
    public CommonResponse<User> updateUser(@PathVariable("id") Integer id, User userDto, @RequestPart("file") MultipartFile multipartFile) {
        return ResponseHelper.ok(userService.updateUser(id, userDto, multipartFile));
    }

//    request yang menjalankan sistem get
    @GetMapping("/{id}")
    public User getById(@PathVariable("id") Integer id) {
        return userService.getById(id);
    }

//    request yang menjalankan get all
    @GetMapping("/all-user")
    public List<User> getAll() {
        return userService.getAll();
    }

//    request yang menjalankan sistem login
    @PostMapping("/sign-in")
    public CommonResponse<Map<String, Object>> login(@RequestBody LoginDto loginDto) {
        return ResponseHelper.ok(userService.login(loginDto));
    }

//    request updateimg
    @PutMapping(consumes = "multipart/form-data",path = "/img/{id}")
    public CommonResponse<User> updateImg(@PathVariable("id") Integer id,@RequestPart("file") MultipartFile multipartFile) {
        return ResponseHelper.ok(userService.updateImg(id, multipartFile));
    }
}
