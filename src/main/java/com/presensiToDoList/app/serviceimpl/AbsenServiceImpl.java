package com.presensiToDoList.app.serviceimpl;

import com.presensiToDoList.app.dto.AbsensiDto;
import com.presensiToDoList.app.enumating.PresensiEnum;
import com.presensiToDoList.app.exception.NotFoundException;
import com.presensiToDoList.app.model.Absen;
import com.presensiToDoList.app.repository.AbsenRepository;
import com.presensiToDoList.app.repository.UserRepository;
import com.presensiToDoList.app.service.AbsenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

//ALUR PEMBUATAN (40)

//membuat implements method
@Service
public class AbsenServiceImpl implements AbsenService {
//    membuat static final untuk mengubah jam dari GMT0 menjadi GMT+7
    private static final Integer hour = 3600 * 1000;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AbsenRepository absenRepository;

//    implements method add atau tambah absen
    @Override
    public Absen absenYuk(AbsensiDto absensi) {
        Absen absen = new Absen();
        if (absensi.getPresensi().name().equals("PULANG"))
            absen.setPresensi(PresensiEnum.PULANG);
        else absen.setPresensi(PresensiEnum.MASUK);
        absen.setUserId(userRepository.findById(absensi.getUserId()).orElseThrow(() -> new NotFoundException("User id tidak ditemukan")));
        absen.setJam(new Date(new Date().getTime() + 7 * hour));
        return absenRepository.save(absen);
    }

//    implements method get absen by id user
    @Override
    public Page<Absen> getAbsen(int page, Integer userId) {
        Pageable pages = PageRequest.of(page, 30);
        return (Page<Absen>) absenRepository.findByUserId(userId, pages);
    }

//    implements method get by id
    @Override
    public Optional<Absen> getId(Long id) {
        return absenRepository.findById(id);
    }

//    implements method delete by id
    @Override
    public void delete(Long id) {
        absenRepository.deleteById(id);
    }
}
