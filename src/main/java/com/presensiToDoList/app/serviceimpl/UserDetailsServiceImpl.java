package com.presensiToDoList.app.serviceimpl;

import com.presensiToDoList.app.model.User;
import com.presensiToDoList.app.model.UserPrinciple;
import com.presensiToDoList.app.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

//ALUR PEMBUATAN (10)

//membuat implements UserDetailsService untuk membuat sistem login
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(username).orElseThrow(() -> new UsernameNotFoundException("Username not found"));
        return UserPrinciple.build(user);
    }
}
