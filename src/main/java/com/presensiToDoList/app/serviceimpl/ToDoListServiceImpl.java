package com.presensiToDoList.app.serviceimpl;

import com.presensiToDoList.app.dto.ListDto;
import com.presensiToDoList.app.exception.NotFoundException;
import com.presensiToDoList.app.model.ToDoList;
import com.presensiToDoList.app.repository.ToDoListReppository;
import com.presensiToDoList.app.repository.UserRepository;
import com.presensiToDoList.app.service.ToDoListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

//ALUR PEMBUATAN (33)

//membuat implements method service
@Service
public class ToDoListServiceImpl implements ToDoListService {
//    membuat method static final untuk mengubah time
    private static final Integer hour = 3600 * 1000;

    @Autowired
    private ToDoListReppository listReppository;

    @Autowired
    private UserRepository userRepository;

//    implements method add list
    @Override
    public ToDoList addList(ListDto list) {
        ToDoList addList = new ToDoList();
        addList.setUserId(userRepository.findById(list.getUserId()).orElseThrow(() -> new NotFoundException("User id tidak ditemukan")));
        addList.setTask(list.getTask());
        addList.setCreatedAt(new Date(new Date().getTime() + 7 * hour));
        return listReppository.save(addList);
    }

//    implements method update lits
    @Override
    public ToDoList updateList(Long id, ListDto list) {
        ToDoList data = listReppository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ditemukan"));
        data.setTask(list.getTask());
        data.setCreatedAt(new Date(new Date().getTime() + 7 * hour));
        return listReppository.save(data);
    }

//    membuat method static final status SELESAI, untuk validasi todolist ketika selesai
    public static final String status = "SELESAI";

//    implements validasi selesai
    @Override
    public ToDoList selesai(Long id) {
        ToDoList data = listReppository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ditemukan"));
        data.setCreatedAt(new Date(new Date().getTime() + 7 * hour));
        data.setStatus(status);
        return listReppository.save(data);
    }

//    implements method get data by user id
    @Override
    public List<ToDoList> getUserId(Integer userId) {
        return listReppository.findByUserId(userId);
    }

//    implements method get data by id
    @Override
    public Optional<ToDoList> getId(Long id) {
        return listReppository.findById(id);
    }

//    implements method delete by id
    @Override
    public void delete(Long id) {
        listReppository.deleteById(id);
    }
}
