package com.presensiToDoList.app.serviceimpl;

import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.presensiToDoList.app.dto.LoginDto;
import com.presensiToDoList.app.dto.UserDto;
import com.presensiToDoList.app.exception.InternalErrorException;
import com.presensiToDoList.app.exception.NotFoundException;
import com.presensiToDoList.app.jwt.JwtProvider;
import com.presensiToDoList.app.model.User;
import com.presensiToDoList.app.repository.UserRepository;
import com.presensiToDoList.app.service.UserService;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//ALUR PEMBUATAN (27)

//membuat implements method di file service
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    JwtProvider jwtProvider;
    @Autowired
    UserDetailsService userDetailsService;
    @Autowired
    PasswordEncoder passwordEncoder;

//    untuk otomatis membuat token ketika login
    private  String authories(String email, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (BadCredentialsException e) {
            throw new InternalErrorException("Email or Password not found");
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(email);
        return jwtProvider.generateToken(userDetails);
    }


//    implements method login
    @Override
    public Map<String, Object> login(LoginDto loginDto) {
        String token = authories(loginDto.getEmail(), loginDto.getPassword());
        User user = userRepository.findByEmail(loginDto.getEmail()).get();
        Map<String, Object> response = new HashMap<>();
        response.put("token", token);
        response.put("expired", "15 menit");
        response.put("user", user);
        return response;
    }

//    method firebase
    private static final String DOWNLOAD_URL = "https://firebasestorage.googleapis.com/v0/b/profile-aa837.appspot.com/o/%s?alt=media";

//    implements method register
    @Override
    public User addUser(User user) {
        var validasi = userRepository.findByEmail(user.getEmail());
        if (validasi.isPresent()) {
            throw new InternalErrorException("Maaf Email sudah digunakan");
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setEmail(user.getEmail());
        user.setUsername(user.getUsername());
        user.setNoHp(user.getNoHp());
        return userRepository.save(user);
    }

//    implements method update
    @Override
    public User updateUser(Integer id, User userDto, MultipartFile multipartFile) {
        String foto = convertToBase64Url(multipartFile);
        User data = userRepository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ditemukan"));
        data.setUsername(userDto.getUsername());
        data.setEmail(userDto.getEmail());
        data.setPassword(passwordEncoder.encode(userDto.getPassword()));
        data.setNoHp(userDto.getNoHp());
        data.setBio(userDto.getBio());
        data.setProfil(foto);
        return userRepository.save(data);
    }

//    method untuk mengubah gambar menjadi string url
    private String convertToBase64Url(MultipartFile file) {
        String url = "";
        try{
            byte[] byteData = Base64.encodeBase64(file.getBytes());
            String result = new String(byteData);
            url = "data:" + file.getContentType() + ";base64," + result;
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            return  url;
        }

    }

//    implements method get by id
    @Override
    public User getById(Integer id) {
        return userRepository.findById(id).get();
    }

//    implements method get all
    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public User updateImg(Integer id, MultipartFile multipartFile) {
        String gambar = convertToBase64Url(multipartFile);
        User data = userRepository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ditemukan"));
        data.setProfil(gambar);
        return userRepository.save(data);
    }

    //    method untuk mengubah gambar menjadi file firebase
    private String imageConverter(MultipartFile multipartFile) {
        try {
            String fileName = getExtensions(multipartFile.getOriginalFilename());
            File file = convertToFile(multipartFile, fileName);
            var RESPONSE_URL = uploadFile(file, fileName);
            file.delete();
            return RESPONSE_URL;
        } catch (Exception e) {
            e.getStackTrace();
            throw new InternalErrorException("Error upload file");
        }
    }
    private File convertToFile(MultipartFile multipartFile, String fileName) throws IOException {
        File file = new File(fileName);
        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(multipartFile.getBytes());
            fos.close();
        }
        return file;
    }
//    method untuk mengupload gambar ke firebase
    private String uploadFile(File file, String fileName) throws IOException {
        BlobId blobId = BlobId.of("profile-aa837.appspot.com", fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("media").build();
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream("./src/main/resources/foto-profile.json"));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        storage.create(blobInfo, Files.readAllBytes(file.toPath()));
        return String.format(DOWNLOAD_URL, URLEncoder.encode(fileName, StandardCharsets.UTF_8));
    }
    private String getExtensions(String fileName) {
        return fileName.split("\\.")[0];
    }

}
