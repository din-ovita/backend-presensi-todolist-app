package com.presensiToDoList.app.configuration;

import com.presensiToDoList.app.jwt.AccesDenied;
import com.presensiToDoList.app.jwt.JwtAuthTokenFilter;
import com.presensiToDoList.app.jwt.UnauthorizedError;
import com.presensiToDoList.app.serviceimpl.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.web.servlet.context.WebApplicationContextServletContextAwareProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

//ALUR PEMBUATAN (16)

//membuat configuration
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private AccesDenied accesDeniedHandler;

    @Autowired
    private UserDetailsServiceImpl userDetailService;

    @Autowired
    private UnauthorizedError unauthorizedError;

//    untuk integrasi request mapping dll di controller
    private static final String[] AUTH_WHITLIST = {
            "/v2/api-docs",
            "/swagger-resources",
            "/swagger-resources/**",
            // -- Swagger UI v3 (OpenAPI)
            "/v3/api-docs/**",
            "/swagger-ui/**",
            "/authentication/**",
            "/",
            "/user/sign-in",
            "/user",
            "/user/img/**",
            "/user/**",
            "/list",
            "/list/**",
            "/list/selesai",
            "/absen",
            "/absen/**",
            "/absen/absen-masuk",
            "/absen/absen-pulang"
    };

    @Bean
    public JwtAuthTokenFilter authTokenFilter() {
        return new JwtAuthTokenFilter();
    }

    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.userDetailsService(userDetailService).passwordEncoder(passwordEncoder());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

//    mengubah password menjadi password acak default
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

//    security
    protected void configure(HttpSecurity http) throws Exception {    http.cors().and().csrf().disable()
            .exceptionHandling().authenticationEntryPoint(unauthorizedError).and()
            .exceptionHandling().accessDeniedHandler(accesDeniedHandler).and()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
            .authorizeRequests()
            .antMatchers(AUTH_WHITLIST).permitAll()
            .anyRequest().authenticated();
        http.addFilterBefore(authTokenFilter(), UsernamePasswordAuthenticationFilter.class);}
}
