package com.presensiToDoList.app.response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

//ALUR PEMBUATAN (18)

//membuat template response yang ditampilkan
public class ResponseHelper {
//    response success
    public static <T> CommonResponse<T> ok(T data) {
        CommonResponse<T> response = new CommonResponse<>();
        response.setMessage("SUCCES");
        response.setStatus("200");
        response.setData(data);
        return response;
    }

//    response error
    public static <T> ResponseEntity<CommonResponse<T>> err(String error, HttpStatus httpStatus) {
        CommonResponse<T> response = new CommonResponse<>();
        response.setStatus(String.valueOf(httpStatus.value()));
        response.setMessage(httpStatus.name());
        response.setData((T) error);
        return new ResponseEntity<>(response, httpStatus);    }
}
